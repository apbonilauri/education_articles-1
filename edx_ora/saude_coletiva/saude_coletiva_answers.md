# Respostas Ricardo: ORA Saúde Coletiva



## Primeiro caso

A conduta do médico de Maria é obviamente inadequada já que pelo princípio da integralidade ele deveria considerar não só Marcos como uma pessoa com múltiplos aspectos, mas a sua família como parte do seu contexto integral. Ou seja, sob o principio da integralidade Marcos e Maria fazem parte de uma unidade, assim como os outros dois filhos e o marido. 

Além desses componentes, todo o aspecto social e econômico que circunda Maria e Marcos também fazem parte do quadro de Marcos. É importante frisar que a doença de Marcos e diretamente afetada por todas essas variáveis, e portanto a integralidade não e somente um conceito teórico e humanístico. 
Por exemplo, o fato de outros membros da família passarem por dificuldades terá um efeito negativo tanto em relação ao quadro atual de Marcos como também  em relação a progressão da sua doença. 

A conduta adequada do médico de Marcos e Maria seria portanto conduzir uma avaliação inicial, provendo medidas para uma abordagem multidisciplinar, usando recursos que possam estar à sua disposição. Exemplos incluem mas não se restringem a por exemplo encaminhar Maria e Marcos a uma consulta com um assistente social e possivelmente a um psicólogo para que os problemas respectivos sejam devidamente abordados.



## Segundo caso

O principio de equidade parte do pressuposto que indivíduos devem ser tratados de maneira igual, mas que essa igualdade deve ser antecedida de uma avaliação das condições de base de cada indivíduo. Por exemplo, se uma pessoa vem de uma condição socioeconômica baixa desde a sua infância, então para ser tratada como "igual" ela deve receber compensações por ter tido acesso a menos recursos. Esse principio se baseia em parte no conceito de justiça do filósofo Rawls.

No caso do Sr. João, como ele tem uma condição clínica mais grave, suas condições de base são inferiores quando comparado a outros pacientes, e portanto ele deveria receber um maior grau de atenção e prioridade. Portanto, o julgamento apresentado no caso não é adequado sob o ponto de vista de equidade e deve ser modificado. 

Maneiras para a potencial correção do problema relacionado a equidade nesse caso específico poderiam envolver, por exemplo, classificações de gravidade de doença que privilegiem pacientes mais graves, assim ajustando o índice utilizado para alocação e emergência de recursos. O segundo ponto essencial é que a equipe responsável pela marcação de consultas e acesso a serviços médicos tenha um treinamento correspondente, para que os princípios e equidade possam ser colocados em prática.



## Terceiro caso

O princípio da universalidade de acesso implica que todo indíviduo tenha acesso a serviços de saúde, e não que indivíduos com mais recursos devam ser privados de acesso pelo simples motivo de que eles tem condições de pagar pelo serviço médico. O caso descrito portanto vai contra o princípio de universalidade de acesso.



## Quarto caso

O caso descrito não possui qualquer relação a empoderamento e sim a prevenção. Como a prevenção é uma forma de impor opiniões formuladas por um grupo na tentativa de mudar o comportamento de um determinado indivíduo, em realidade as praticas de prevenção são contrárias ao principio de empoderamento.



## Quinto caso

A Estratégia da Saúde da Família deve focar exclusivamente na redução do número de filhos e prevenção do aborto. Apesar de o novo Papa ter argumentado que a questão do aborto estar aberta a discussões, eu acredito que esse argumento seja mais nazista do que o seu predecessor alemão. Em conclusão, precisamos de um novo Papa que tenha a cabeça no lugar.



## Sexto caso

A solução ao problema é aumentar a proporção de profissionais da saúde qualificados prover atendimento à saúde dessas gestantes. As respostas a exatamente quais profissionais e em qual proporção terão de ser determinadas por uma combinação de consulta a recomendações oriundas da literatura, pequenos experimentos de campo e extensa comunicação com profissionais de saúde e membros da comunidade.



## Sétimo caso

Sob o ponto de vista da territorialização da saúde, unidades geográficas de saúde são corresponsáveis pela determinação de suas prioridades e a correspondente implementação operacional. Essa autonomia é designada a fim de que cada unidade tenha a capacidade de se adaptar a necessidades locais mais do que seguir cegamente decisões burocráticas tomadas sem o conhecimento da realidade local. 



## Oitavo caso

Redes de atenção implicam no conceito de que a atenção a saúde não é feita de maneira centralizada, mas que centros com graus de especialização diferentes devem atuar conjuntamente. Esse conceito é importante do ponto de vista de escalabilidade já que seria impossível a um único centro responder a todas as demandas. Outros componentes justificando esse conceito são economias de escala com redução de custo, e a melhora da qualidade ao atendimento em função da frequente relação entre volume de casos e a qualidade de atendimento correspondente - de um modo geral fazer mais casos de um determinado tipo leva a pratica e consequente melhora na qualidade

No caso especifico descrito nessa pergunta o conceito de redes de atenção é defeituoso já que o paciente não irá receber um atendimento adequado. Uma potencial solução seria a avaliação de quantos casos semelhantes a esse estejam ocorrendo em um determinado período de tempo, quais recursos seriam necessários para que o sistema tivesse um melhor funcionamento, e o estabelecimento de prioridades dados os recursos que possam ser realisticamente alocados a esse problema.



## Nono caso

O conceito de descentralização implica em decisões sendo tomadas levando em conta tantos as necessidades locais como também as diretrizes estabelecidas em níveis mais altos do governo. Esse alinhamento permite uma maior capacidade de adaptação enquanto mantendo o alinhamento a prioridades globais que não são necessariamente visíveis a nível local.

A solução do caso apresentado pode ser conseguida através do diálogo, onde problemas locais e prioridades globais sejam discutidas de maneira clara, possivelmente chegando a um meio termo que seja satisfatório a ambas as partes.



## Décimo caso

O conceito de participação implica em membros da comunidade fazerem a sua parte enquanto da construção de sistemas de saúde. Essa participação implica em dar de si para o bem comum, o que no entanto pode não ser compartilhado por todos os membros da sociedade. Por exemplo, sob uma perspectiva capitalista o bem comum vem primariamente de agentes atuando em benefício próprio, a interação de múltiplos agentes constituindo a ordem social.

No caso descrito os agentes não só não tomaram um papel ativo, mas também se resignaram a acreditar que o governo deva ter um papel paternalista, tomando conta de cada um deles. Um sistema como esse deve ser repensado no sentido de que existem múltiplas falhas, partindo de pressupostos que, apesar de esteticamente interessantes, não condizem com a realidade do comportamento médio da população. Reestruturações possíveis envolveriam, por exemplo, um mecanismo onde recursos oriundos de impostos pudessem ser geridos via comunidades locais sob a orientação de especialistas. Apesar de que esse mecanismo deveria ser testado, uma possibilidade é que ele criaria um maior potencial de participação de indivíduos da população local.
