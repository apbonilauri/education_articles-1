# Global adult skill observatory: Evaluating lifelong learning and national innovation through a reproducible research framework


Mailson Santos   
Jacson Barros  
Joao Vissoci  
Mauricio Maldonado  
Sarven Capadisli  
Elias Carvalho  
Ricardo Pietrobon  


<!-- 

http://www.oecd.org/edu/a-zindexofdirectorateforeducationwork.htm  A-Z index of Directorate for Education and Skills Work

 -->

<!-- http://oscarperpinan.github.io/spacetime-vis/spacetime.html

 -->

<!-- 
* acho que relevancia e o ponto principal, e entao eu vou pegar dois artigos do OECD que falam sobre long-life learning http://goo.gl/Oo7JbM e adult skills http://goo.gl/c0RqZv e usar esses topicos como base pra selecionar variaveis que venham a dar apoio pra politicas de educacao nessas areas. a coisa interessante dessas areas e que elas se relacionam a producao economica do pais, inovacao e outras coisas que o Mauricio conhece super bem
* quanto a existencia de dados poderiamos ter um script onde paises que nao tenham uma quantia minima de dados sejam excluidos pra aquela analise - isso vai evitar que os educadores tenham de ficar cacando graficos que funcionem como no caso do Sarven
* vai ser importante gerar mapas, mas isso e facil
* como essas areas nao tem muitos dados, vamos ter de incluir coisas de outros lugares. isso pro artigo e bom porque enriquece os dados que vao vir do Sarven. idealmente vamos pegar coisas em xml, json, e texto livre minerado
* em principio nos poderiamos ter outros tipos de analise de data mining mais sofisticadas rodando em tempo real, mas vamos ver 
* uma coisa adicional interessante seria um feed onde os artigos relevantes para o tema fossem inseridos junto dos graficos via a ontologia CiTO, mas aqui precisamos pensar um pouco pra ver se isso poderia ser inserido de uma maneira que cresca pelo menos semi-automaticamente ao longo do tempo

enfim, nos proximos dias dou um retorno em relacao ao mapeamento banco mundial de um lado e adultskills/lifelong learning do outro. Jacson, voce poderia me mandar uma lista das classes disponiveis em 

[OECD Linked Data](http://oecd.270a.info/.html)
[International Monetary Fund Linked Data](http://imf.270a.info/.html)

 -->


<!-- 
(school|grade|primary|secondary|tertiary|education|teacher|pupil|classroom|learning|mathematics|reading|numeracy|score|academic|enrol*ment|liter|intake|enrol*ment)

[0-9].*(i">)



 -->
<!-- list of graphics in observatory aligned with rcharts examples -->

<!-- next papers - explore semantic citations about policy making - jstor?? -->

<!-- ability to download data -->

<!-- Jacson's example  -->

<!--
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX wgs: <http://www.w3.org/2003/01/geo/wgs84_pos#>
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX dbp: <http://dbpedia.org/property/>
PREFIX dbr: <http://dbpedia.org/resource/>
PREFIX sdmx: <http://purl.org/linked-data/sdmx#>
PREFIX sdmx-attribute: <http://purl.org/linked-data/sdmx/2009/attribute#>
PREFIX sdmx-dimension: <http://purl.org/linked-data/sdmx/2009/dimension#>
PREFIX sdmx-measure: <http://purl.org/linked-data/sdmx/2009/measure#>
PREFIX qb: <http://purl.org/linked-data/cube#>
PREFIX year: <http://reference.data.gov.uk/id/year/>
PREFIX void: <http://rdfs.org/ns/void#>

PREFIX wbld: <http://worldbank.270a.info/>
PREFIX property: <http://worldbank.270a.info/property/>
PREFIX classification: <http://worldbank.270a.info/classification/>
PREFIX indicator: <http://worldbank.270a.info/classification/indicator/>
PREFIX country: <http://worldbank.270a.info/classification/country/>
PREFIX income-level: <http://worldbank.270a.info/classification/income-level/>
PREFIX lending-type: <http://worldbank.270a.info/classification/lending-type/>
PREFIX region: <http://worldbank.270a.info/classification/region/>
PREFIX source: <http://worldbank.270a.info/classification/source/>
PREFIX topic: <http://worldbank.270a.info/classification/topic/>
PREFIX currency: <http://worldbank.270a.info/classification/currency/>
PREFIX project: <http://worldbank.270a.info/classification/project/>
PREFIX loan-status: <http://worldbank.270a.info/classification/loan-status/>
PREFIX variable: <http://worldbank.270a.info/classification/variable/>
PREFIX global-circulation-model: <http://worldbank.270a.info/classification/global-circulation-model/>
PREFIX scenario: <http://worldbank.270a.info/classification/scenario/>

PREFIX d-indicators: <http://worldbank.270a.info/dataset/world-bank-indicators>
PREFIX d-finances: <http://worldbank.270a.info/dataset/world-bank-finances/>
PREFIX d-climates: <http://worldbank.270a.info/dataset/world-bank-climates/>

#USE THESE GRAPHS :)
PREFIX g-void: <http://worldbank.270a.info/graph/void>
PREFIX g-meta: <http://worldbank.270a.info/graph/meta>
PREFIX g-climates: <http://worldbank.270a.info/graph/world-bank-climates>
PREFIX g-finances: <http://worldbank.270a.info/graph/world-bank-finances>
PREFIX g-projects: <http://worldbank.270a.info/graph/world-bank-projects-and-operations>
PREFIX g-indicators: <http://worldbank.270a.info/graph/world-development-indicators>


SELECT *
WHERE {
GRAPH g-indicators: {
?s property:indicator indicator:NY.GDP.MKTP.KD.ZG .
?s sdmx-dimension:refPeriod ?period .
?s sdmx-dimension:refArea ?country .
?s sdmx-measure:obsValue ?obsValue .
BIND (substr( str(?period),38,4) as ?year )
FILTER ( ?country
           IN ( country:US , country:CA , country:MX  )
).

FILTER ( xsd:int(?year) >= 2000 && xsd:int(?year) <= 2008 )
}
}
ORDER BY ?year
 -->

<!-- http://www.w3.org/2011/gld/wiki/Data_Cube_Vocabulary

 -->

<!--
http://goo.gl/dCFCsb
 -->

<!--
reports http://goo.gl/QsCZfg
oecd time series country data http://goo.gl/QsCZfg
 -->

<!--
RDF
http://270a.info/
http://stats.270a.info/

 -->

<!--
didn't include OECD with change rates
  -->

<!--

https://gist.github.com/rpietro/6590113
 -->

<!-- focus on risk adjusted scores and on communicating in alignment with local political interests in each country - has to figure how to get that with a simple rule

 -->

<!--

1. primary objective of the learning repository is to provide adjusted educational scores http://goo.gl/SYH5Lj in the form of time series (we will not consider data for a single year) with information on individual countries (we will not get data from single countries). this definition is important since the requirement that we must have time series and multiple countries will make us discard a lot of data, ultimately having an inclusion criteria for what goes into the repository

2. since we have time series and multiple countries, our graphics will likely include apps that can display dynamic time series charts as well as maps (below i will refer to them as "dynamic graphic apps"). dynamic graphic apps can be easily built with the shiny http://goo.gl/Rn9X6 , googlevis http://goo.gl/n9DoH and rcharts http://goo.gl/YXFMrT packages

3. how the learning observatory differentiates itself from other large repositories is that we will take one central dataset as our starting point and then enrich it with other data sources in whatever format they might be: json, xml, csv, free text (to be mined), and rdf

4. our baseline data will be the World Bank data sets, since not only they have an extensive number of indicators for several countries and years http://goo.gl/dm4zH , their data is freely available (although there are some licensing restrictions), they already have an API http://goo.gl/Crh5lu , and that API already has an R wrapper http://goo.gl/N34QkT

5. although the world bank API is outstanding, it does pose a challenge in the architecture of the learning observatory in that rather than storing all the data locally, we would have to integrate the world bank into our architecture. although i think that the architecture should be the primary focus of our meeting, one option would be as follows:

* the learning observatory will be centered on putting one dynamic graphic shiny app at a time (a dynamic graphic being created with shiny). this means that we will pull data in as needed for each graphic app, perhaps stored within mongodb just to take advantage of the flexibility allowed for its schemaless structure. for example, if we decide that we will compare tertiary graduation rates and number of startups in the country, then we pull all the data into our local database for that specific dynamic graphic and deploy

* in order to put a specific dynamic graphic together, we simply determine which additional data sets might be required and bring them in. for example, yesterday I collected 70 files from OECD with time series that could be aggregated to the world bank data as well as a collection of articles that can be mined using a zoom-in-out methodology with jabref/regexp/text mining http://goo.gl/qykadJ as well as referenced along the dynamic graphic

6. advantages of this architecture are the following

* we can start immediately, meaning that the Learning Observatory is built one dynamic graphic app at a time. this means that, for example, if Mauricio wants to start a paper tomorrow or if I want to write something about lifelong skill education, we just work on building a data set that we can use immediately and then post it as a dynamic graphic app in our site. in relation to the paper describing the learning repository, after we aggregate three or four dynamic graphic apps the paper is ready to be sent to a journal. in sum, we will be very agile and quickly build a minimum viable product. actually, i suggest that we build the first dynamic graphic app and corresponding data set to Mauricio so that he can start a parallel paper, but let's discuss during our meeting

* although we will be dealing with huge data sets, the data set for each dynamic graphic app will be relatively small, making our server requirements far lighter

* one risk with a project like this is that we build something cool but policy makers don't use it and the whole thing becomes just an academic exercise. if instead we focus on building one dynamic graphic app at a time, we can do that in a way that will target the needs of specific education policy makers in different countries. Joao, Elias, Jacson and I are already working on putting togehter a facebook page for Brazil where we will bring together a bunch of students and education specialists (among them education policy makers). it's our plan to repeat this for other countries once we get the brazilian page to work, and so the facebook pages combined with my blog to disseminate these dynamic graphic pages could be the perfect channel to spread these dynamic graphic apps
 -->


<!--
https://hackpad.com/Scraping-Websites-to-Collect-Consumption-and-Price-Data-SXoIdM1XFyw
http://bigdata.infochimps.com/Portals/174427/docs/cloudstreams-whitepaper.pdf
https://github.com/quandl/R-package
https://github.com/vincentarelbundock/countrycode
http://www.infochimps.com/datasets
 -->

## Abstract
<!-- will write at the end -->

## Introduction

Education is arguably one of the most important determinants of development for a country (ref). And yet, while agencies such as the [OECD (Organisation for Economic Co-operation and Development)]() constantly release data and reports on selected countries, to our knowledge no previous efforts have focused on data aggregation followed by the dynamic graphic presentation of results measuring the association between educational indices and other indicators of national economic and healthcare development.

* Educational indicators
* Association with economic and healthcare indicators

The objective of this study is therefore to create an Educational Observatory displayed through a business intelligence engine where the association between education and other relevant national development indicators is presented to


## Methods

<!-- go agile
take any two simple datasets and go through the whole process, then aggregate more as needed for first paper

drive design backwards from best observatory scenario - google for what is sorely missing

 -->

### Use cases

<!-- 
lifelong learning and adult skills
innovation parameters
http://goo.gl/K1u6bt
 -->

* Education policy makers access dynamic graphics to investigate associations between educational indices and socio-economic and healthcare indices for their own countries in comparison with other countries
* Education policy makers are able to download data for each of the graphics in a spreadsheet format

<!-- future:
able to
 -->


### Data sources


* [OECD Linked Data](http://oecd.270a.info/.html)
* [World Bank Linked Data](http://worldbank.270a.info/.html)
* [International Monetary Fund Linked Data](http://imf.270a.info/.html)
* [World Health Organization](http://apps.who.int/gho/data/view.main)
* [thinknum](http://thinknum.com/)
* [Quandl education](http://www.quandl.com/education#List+of+Education+Indicators+Across+Countries)
* [Quandl health](http://www.quandl.com/health#List+of+Health+Indicators+Across+Countries)


```{r}
Quandl.auth("wyoGMo7Wi32j6mgnY8zy") # this is my token, please grab yours at http://www.quandl.com/users/edit (you need to get an account first)
library(devtools)
install_github('R-package','quandl')
require(Quandl)
require(ggplot2)
require(RCurl)

data <- getURL("https://gist.github.com/rpietro/6665635/raw/edb700cc7b2110c3f7dd4d559221f351e17369fd/ed.csv")
ed <- read.csv(data)
head(ed)
qplot(ed$X.5Y.Ago, ed$Level)

Quandl.search("Education") #searches will only work after you have entered your token -- see above

# Education - there doesnt seem to be any global educational datasets, which means we would have to escrape pages if we decide to use Quandl as a sour
# Code: UKONS/L2PA
# Desc: seasonal_adjustment='SA', base_period='2009', price='CONS', index_period='2009'. The Blue Book is a key annual publication of National Accounts statistics and the essential data source for anyone concerned with macro-economic policies and studies. These National Statistics are produced to high professional standards and released according to the arrangements approved by the UK Statistics Authority.
# Freq: annual
# Cols: Year|Value

# Educational Services Earnings in Alabama
# Code: FRED/ALEEDU
# Desc: Millions of Dollars Not Seasonally Adjusted, Industry based on 2002 North American Industry Classification System (NAICS). Please consult the U.S. Census Bureau (http://www.census.gov/epcd/naics02/naicod02.htm) for additional information about NAICS.
# Freq: quarterly
# Cols: Date|Value

# Educational Services Earnings in Delaware
# Code: FRED/DEEEDU
# Desc: Millions of Dollars Not Seasonally Adjusted, Industry based on 2002 North American Industry Classification System (NAICS). Please consult the U.S. Census Bureau (http://www.census.gov/epcd/naics02/naicod02.htm) for additional information about NAICS.
# Freq: quarterly
# Cols: Date|Value

```


```{r}

# script from http://goo.gl/ui40Yq
install.packages("Thinknum")
require("Thinknum")
goog <- Thinknum("goog")
plot(goog, type="l")

mThinknum <- function(command, tall=F) {

  require("Thinknum")

  # Break the command into seperate calls
  think.list <- strsplit(command, ";")[[1]]

  # Look through each element of the think.list
  for (i in 1:length(think.list)) {

    cat(paste("Reading:",think.list[i])) # Display feedback

    if (!tall) {
      if (i==1) returner <- Thinknum(think.list[1])
      if (i>1)  returner <- merge(returner,Thinknum(think.list[i])
                                , by.x="date_time", by.y="date_time")
    }
    if (tall) {
      tempdat <- Thinknum(think.list[i])
      names(tempdat) <- c("date_time", "value")

      if (i==1) returner <- data.frame(tempdat, call=think.list[i])
      if (i>1) returner <- rbind(returner, data.frame(tempdat, call=think.list[i]))
      names(returner) <- c("date_time", "value", "call")
    }

    cat(paste(rep(" ", max(1,20-nchar(think.list[i]))), collapse=""))
      # Insert spaces
    cat(paste("Dimensions:", paste(dim(returner), collapse="x"), "\n"))
      # Show dimensions
  }
  # Ensure the return file has appropriate column names
  if (!tall) names(returner) <- c("date_time", think.list)

  data.frame(returner)
}

test <- mThinknum("^spx;sma(^spx,30);sma(goog*2,30);(goog*2)")
head(test)

  # Let's try plotting with the base package

  plot(x=c(min(test$date_time), max(test$date_time)),
       y=c(min(test[,2:5]),max(test[,2:5])), type="n",
       main="Plot in R")

  lines(test$date_time, test[,2], type="l")
  lines(test$date_time, test[,3], type="l", col="red")
  lines(test$date_time, test[,4], type="l", col="blue")
  lines(test$date_time, test[,5], type="l", col="darkgreen")

```

<!--

to be checked:

http://db.library.duke.edu/
http://thedatahub.org/
http://apps.who.int/
http://www.internationalsurveynetwork.org/
http://www4.wiwiss.fu-berlin.de/drugbank/
http://www.cdc.gov/nchs/
http://sedac.ciesin.columbia.edu/povmap/
http://www.icpsr.umich.edu/icpsrweb/ICPSR/
http://www.afrobarometer.org/data/
http://globalpolicy.gmu.edu/genocide/

 -->

### Assembly, storage and merging
* OECD, WB and IMF from http://270a.info/
* WHO, thinksum and Quandl linked through MongoDB
* Country taxonomy <!-- need to find standard and then bring all databases into the same taxonomy -->


### Exploratory data mining and modeling

* use [MINE](http://www.exploredata.net/) for initial association measurement -
* free text
    * [Weka](http://www.cs.waikato.ac.nz/ml/weka/) through rattle for data mining
    * NLTK python
* [R](http://www.r-project.org/) and [rattle](http://rattle.togaware.com/)


### Modeling
* [PMML (Predictive Model Markup Language)](http://www.dmg.org/v4-1/GeneralStructure.html)
* [LSA](http://cran.r-project.org/web/packages/lsa/index.html)


### Business Intelligence and dynamic graphics

* [Kettle](http://kettle.pentaho.com/)


### Reproducible research
* github R scripts
* figshare and github
* mongodb query examples
* release as reproducible and through PMML
* other data sets released depending on original license


## Results


Table 1. Observatory contents

<!-- data dictionary in github and figshare - data dictionary with multiple versions and corresponding citations -->


Figure 1. MINE associations of interest


Figure 2. Education and economic indicators


Figure 3. Education and healthcare indicators


Figure 4. Business intelligence interface
<!-- draw BI - check with Mailson about d3.js within kettle or what exactly we might want to use -->



## Discussion
* first education observatory covering the association between education metrics and a series of national indicators.

* progressively harvest more and more data - store tons of text data and

### Primacy and summary

### Result 1
### Result 2
### Result 3
### Result 4
### Limitations

### Future
