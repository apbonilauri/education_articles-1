# rprior test

#Joao, #? means that the role of the previous code is unknown, our first goal is to first distribute comments throughout the file and then progressively clarify what each one of the $? mean
MAT <-
function(ipar, # a data frame containing M3PL item parameters, specifically a1, a2, ... , d, and c
              resp, #a data frame (that will be converted to a numeric matrix) of item responses, e.g., R1, R2, ..., R180
              cors, #a square matrix of the lower diagonal elements of a variance-covariance (VCV) matrix, including 1s in the main diagonal #? not really sure what this VCV contains, but seems like the correlation among the different dimensions
              target.content.dist=NULL, #an optional vector of target content distributions summed to 1.0, e.g., c(0.25,0.5,0.25), this means the distribution in total number of items, with each number representing its proportion, which therefore has to sum up to 1 
              content.cat=NULL, #an optional vector specifying content designations
              ncc=1, #the number of content categories (default=1, i.e., no content balancing)
              p=stop("p is required"), #the number of latent dimensions
              selectionMethod=c("D","A","C","R"), #item selection criterion: "D"=D-optimality, "A"=A-optimality, "C"=C-optimality, "R"=Random (default="D")
              selectionType=c("FISHER","BAYESIAN"), #item selection method type: "FISHER"=Fisher information, "BAYESIAN"=adds inverse prior VCV
              c.weights=NA, #an optional vector of weights of length p when selectionMethod="C"
              stoppingCriterion=c("CONJUNCTIVE","COMPENSATORY"), # stopping criterion: "CONJUNCTIVE"=SEs for all dimensions must be met, "COMPENSATORY"=the generalized variance or SEs weighted by c-weights must be met
              topN=1, #Randomesque exposure control: selects an item randomly from the top N most informative items (default=1, no exposure control)
              minNI=10, #minimum number of items to administer (default=10)
              maxNI=30, #maximum number of items to administer (default=30)
              minSE=0.3, #minimum SE for stopping (default=0.3)
              D=1.0, #scaling constant: 1.7 or 1.0 (default=1.0)
              maxIter=30,# maximum number of Fisher scoring (default=30)
              conv=0.001,# convergence criterion for Fisher scoring (default=0.001)
              minTheta=-4,# minimum theta value for plotting (default=-4)
              maxTheta=4, #maximum theta value for plotting (default=4)
              plot.audit.trail=T,#show CAT audit trail: T or F (default=T)
              theta.labels=NULL,#theta labels for plotting (default=c("Theta 1","Theta 2",...))
              easiness=T) #logical, T if d is related to the easiness of items per Reckase, F otherwise, same as discrimination ability see http://goo.gl/o1ALj
{

call<-match.call()

if (is.data.frame(ipar)) ipar<-cbind(as.matrix(ipar[paste("a",1:p,sep="")]),as.matrix(ipar["d"]),as.matrix(ipar["c"])) #converting item parameter info from data frame to a group of matrices

if (!easiness) ipar[,p+1]<- -ipar[,p+1] # easiness is discrimination ability, is it converting negative slopes??

if (is.data.frame(resp)) resp<-as.matrix(resp) #same conversion to matrix format for responses

selectionMethod<-toupper(selectionMethod) # toupper simply correcting a potential syntax spelling error by the user
selectionMethod<-match.arg(selectionMethod) # match.arg matches arg against a table of candidate values as specified by choices, where NULL means to take the first one.
stoppingCriterion<-toupper(stoppingCriterion)
stoppingCriterion<-match.arg(stoppingCriterion)
selectionType<-toupper(selectionType) #item selection method type: "FISHER"=Fisher information, "BAYESIAN"=adds inverse prior VCV, simply translating characters in character vectors, in particular from upper to lower case or vice versa.
selectionType<-match.arg(selectionType) #matching the entered option against FISHER or BAYESIAN

if (p==1) sigma<-1 #p is the number of latent dimensions
      else if (p>1) {
sigma<-as.matrix(cors) # cors is a square matrix of the lower diagonal elements of a variance-covariance (VCV) matrix, including 1s in the main diagonal
sigma[upper.tri(sigma)] <- t(sigma)[upper.tri(sigma)] #transpose of the upper triangular part of the matrix - as for the reason why transpose check http://goo.gl/cgkJQ 
if (dim(sigma)[1]!=dim(sigma)[2] || p!= dim(sigma)[1]) stop("ERROR: p and cors non-conforming") # || means a logical OR, p is the number of latent variables, also sigma is the number of latent constructs as well as the number of dimensions in the matrix, meaning that the matrix establishes the association between any two constructs
} 
      else stop("ERROR: p and cors non-conforming")

if (selectionMethod=="C") { #item selection criterion: "D"=D-optimality, "A"=A-optimality, "C"=C-optimality, "R"=Random (default="D")
if (sum(c.weights)!=1 || length(c.weights) !=p) {
selectionMethod="D" #looks like some kind of exception handling, where if it doesn't conform to some pre-established rules it will default to random choice rather than using other criteria
c.weights<-rep(1/p,p)
warning("ERROR: c.weights do not sum to 1.0 or have unexpected values - D optimality will be used")
}
}

ni<-nrow(ipar) #number of items
nj<-nrow(resp) #number of responses

available.items<-rep(TRUE,ni) #whether a certain item is available or not

content.balancing<-F #initially setting content balancing to false

if (ncc>1 && !is.null(target.content.dist) & !is.null(content.cat)){ #ncc is the number of content categories, target.content.dist an optional vector of target content distributions summed to 1.0, e.g., c(0.25,0.5,0.25) 

if (abs(sum(target.content.dist)-1)>.1) warning("ERROR: target content proportions does not add up to 1.0\n:content balancing will not be used") #this is a strange way to test if number < 0
else if (length(target.content.dist)!=ncc) warning("ERROR: ncc does not match the number of target proportions\n:content balancing will not be used") #exception handling to keep going with code even if content distribution vector is wrongly specified
else if (length(content.cat)!=ni) warning("ERROR: number of rows in the content control file does not match the number of items in the bank\n:content balancing will not be used")
else {
overall.content.freq<-numeric(ncc)
content.balancing<-T #if everything matches then content is balanced, which reverts initial variable value

if (any(target.content.dist<=0)) {
available.items[which(content.cat %in% which(target.content.dist<=0))]<-FALSE #didn't quite get this, but seems to mean that if a content category is set to 0, then available.items is FALSE
}
}
}

next.content<-function() {
available.content<-which(target.content.dist>0) #which asks whether the following is true, target.content.distan optional vector of target content distributions summed to 1.0, e.g., c(0.25,0.5,0.25), this means the distribution in total number of items, with each number representing its proportion, which therefore has to sum up to 1 
idx<-which.max(target.content.dist[available.content]-current.content.dist[available.content]) #which.max determines the location, i.e., index of the first maximum of a numeric vector. ?not sure what the whole thing means, but it might be related to item choice, selecting from target content distribution
return(available.content[idx])
}

update.content.dist<-function() {
idx<-content.cat[item.selected]
current.content.freq[idx]<<-current.content.freq[idx]+1
overall.content.freq[idx]<<-overall.content.freq[idx]+1
current.content.dist<<-current.content.freq/ni.given
}

selectItem<-function(ipar,available,given,th,p,sigma,D=1.7,method="D",c.weights=NA,content.balancing=F,topN=1) {
if (p != length(th)) stop("th and p are non-conforming")
if (sum(available)==0) stop("no available items to be selected")

ni<-nrow(ipar)

info.given<-matrix(0,p,p)

if (sum(given)>0) info.given<-makeFI(ipar[which(given),],th,p,sigma,D=D,add.sigma=F)

if (p==1) sigma.inv<-1
else sigma.inv<-solve(sigma)

info<-rep(0,ni)

if (content.balancing) {
content.available<-available & (content.cat==next.content())
if (sum(content.available)>=1) available<-content.available
}

for (i in 1:ni) {
if (available[i]) {
if (method=="R") info[i]<- runif(1) #random selection
else {
info.i<-calcFI(ipar[i,],th,p,D=D)
info.matrix<-info.given+info.i
if (selectionType=="BAYESIAN") info.matrix<-info.matrix + sigma.inv
if (method=="D") info[i]<-det(info.matrix) #D-optimality: maximizing
else if (method=="A") info[i]<- 1/sum(diag(ginv(info.matrix))) #A-optimality: minimizing
else if (method=="C") info[i]<- -matrix(c.weights,1,p)%*%solve(info.matrix)%*%matrix(c.weights,p,1) #C-optimality: minimizing
}
}
}
info.index<-rev(order(info))
if (topN > 1 && sum(available) > topN) item.selected<-info.index[sample(topN,1)]
else item.selected=which(info==max(info,na.rm=T))[1]
return(item.selected)
}

estimates.full<-SCORE(ipar,resp,p,sigma,maxIter=maxIter,conv=conv,D=D,Fisher=T)

TH<-matrix(nrow=nj,ncol=p)
SE<-matrix(nrow=nj,ncol=p)

start.theta<-rep(0,p)

items.used<-matrix(NA,nj,maxNI)
selected.item.resp<-matrix(NA,nj,maxNI)
ni.administered<-numeric(nj)
theta.CAT<-matrix(NA,nj,p)
se.CAT<-matrix(NA,nj,p)
theta.history<-array(NA,c(nj,maxNI,p))
se.history<-array(NA,c(nj,maxNI,p))

if (plot.audit.trail) dev.new(record=T,width=10,height=6.5)
if (is.null(theta.labels)) theta.labels<-paste("Theta",1:p,sep=" ")

for (j in 1:nj) {
se.met<-logical(p)
theta.current<-start.theta
rs<-resp[j,]

W<-matrix(0,nrow=p,ncol=p) #initialize

crit.met<-FALSE
items.available<-available.items

for (i in 1:ni) {
if (is.na(rs[i]) || (rs[i] !=0 && rs[i] !=1)) items.available[i]<-FALSE
}
items.given<-rep(FALSE,ni)

max.to.administer<-ifelse(sum(items.available)<=maxNI,sum(items.available),maxNI)
ni.given<-0

if (content.balancing) {
current.content.dist<-numeric(ncc)
current.content.freq<-numeric(ncc)
}

while (crit.met==FALSE && ni.given<max.to.administer) {
item.selected<-selectItem(ipar,items.available,items.given,theta.current,p,sigma,D=D,method=selectionMethod,c.weights=c.weights,topN=topN,content.balancing=content.balancing)
ni.given<-ni.given+1
items.used[j,ni.given]<-item.selected

if (content.balancing) update.content.dist()

items.available[item.selected]<-FALSE
selected.item.resp[j,ni.given]<-resp[j,item.selected]
                  estimates<-score(ipar[items.used[j,1:ni.given],,drop=F],rs[items.used[j,1:ni.given]],theta.current,p,sigma,maxIter=maxIter,conv=conv,D=D,Fisher=T)

theta.current<-estimates$theta
theta.history[j,ni.given,]<-theta.current
se.history[j,ni.given,]<-estimates$SE

if (stoppingCriterion=="CONJUNCTIVE") se.met<-all(estimates$SE<=minSE)
else if (stoppingCriterion=="COMPENSATORY") {
V<-solve(-estimates$Hessian)
if (selectionMethod=="C") cV<-sqrt(abs(matrix(c.weights,nrow=1)%*%V%*%matrix(c.weights,ncol=1)))
else cV<-sqrt(abs(matrix(rep(1/p,p),nrow=1)%*%V%*%matrix(rep(1/p,p),ncol=1)))

se.met<-cV<=minSE
}

if (ni.given>=max.to.administer || (se.met && ni.given>=minNI)) {
crit.met<-TRUE
theta.CAT[j,]<-estimates$theta
se.CAT[j,]<-estimates$SE
ni.administered[j]<-ni.given
}
}

if (plot.audit.trail) {
plot(1:maxNI,seq(minTheta,maxTheta,length=maxNI),main=paste("CAT Audit Trail - Examinee ",j,sep=""),xlab="Items Administered",ylab="Theta",type="n",las=1,bg="white")
idx<-0
for (h in p:1) {
idx<-idx+1
points(1:ni.given,theta.history[j,1:ni.given,h],type="b",pch=8+h,lty=h,col=h)
abline(h=estimates.full$theta[j,h],lty=h,col=h)
text(1,minTheta+0.3*(idx),paste(theta.labels[h]," : ",sprintf("%6.3f",theta.CAT[j,h])," SE: ",sprintf("%5.3f",se.CAT[j,h])),cex=0.8,adj=0);
}
if (stoppingCriterion=="COMPENSATORY") text(1,minTheta+0.3*(idx+1),paste("cSE=",round(cV,digits=3),sep=""),cex=0.8,adj=0)
if (p>1) legend("bottomright",theta.labels,col=1:p,lty=1:p,pch=8+1:p,bg="white")
else if (p==1) {
for (i in 1:ni.given) {
lines(rep(i,2),c(theta.history[j,i,1]-1.96*se.history[j,i,1],theta.history[j,i,1]+1.96*se.history[j,i,1]),col="blue")
}
}
item.string<-paste(items.used[j,1:ni.given],collapse=",")
text(1,maxTheta,paste("Items: ",item.string,sep=""),cex=0.7,adj=0)

resp.string<-paste(selected.item.resp[j,1:ni.given],collapse=",")
text(1,maxTheta-(maxTheta-minTheta)/20,paste("Responses: ",resp.string,sep=""),cex=0.7,adj=0)

if (content.balancing) text(1,maxTheta-(maxTheta-minTheta)/10,paste("Content Distribution:",paste(round(current.content.dist,digits=2),collapse=",")),cex=0.7,adj=0)

}
}

if (content.balancing) {
overall.content.dist<-overall.content.freq/sum(overall.content.freq)
content.dist<-rbind(target.content.dist,overall.content.dist)
par.mar<-par()$mar
par(xpd=T,mar=par()$mar+c(0,0,0,4))
barplot(content.dist,ylab="proportion",xlab="content category",las=1,names.arg=paste(1:ncc),col=c("black","grey"),beside=T)
legend(ncc*3+0.2,max(target.content.dist,current.content.dist)/2,c("Target","Current"),fill=c("black","grey"))
par(xpd=F,mar=par.mar)
}

dev.new(record=T,width=10,height=6.5)
par(mfrow=c(2,3))
for (h in 1:p) {
plot(estimates.full$theta[,h],theta.CAT[,h],xlim=c(minTheta,maxTheta),ylim=c(minTheta,maxTheta),xlab=paste("Full Bank",theta.labels[h]),ylab=paste("CAT Theta",theta.labels[h]),col="blue")
text(minTheta,maxTheta,paste("r =",sprintf("%5.3f",cor(estimates.full$theta[,h],theta.CAT[,h]))),adj=0)
abline(0,1)
}


out<-list(call=call,items.used=items.used,selected.item.resp=selected.item.resp,ni.administered=ni.administered,
                theta.CAT=theta.CAT,se.CAT=se.CAT,theta.history=theta.history,se.history=se.history,theta.Full=estimates.full$theta,se.Full=estimates.full$SE,ipar=ipar,p=p)

class(out)<-"MAT"

return(out)
}
