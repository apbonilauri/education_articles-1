# Introduction to divein&copy; Coaching

<!-- rprior -->
## Market pull for divein&copy;

## But what is divein&copy;?

PreciseSkills creates and delivers immersive, personalized, and market-driven online courses where students are trained in a context simulating the demands they will face when applying their future skill.

## Who should read this book?
Educators in both the academic, corporate, and entrepreneurial environments.
Education managers
Those involved with coaching

Although we do cover more complex concepts such as Item Response Theory and Computerized Adaptive Testing, these topics are treated in a non-technical way, with plenty of examples to illustrate each concept.
<!-- think about adding js apps to illustrate different concepts, apps being placed on our server -->



## How this book was written

* small posts in markdown in public github distributed across blogs using situated schema theory
* videos demonstrating how R does it posted as social net blogs
* mark posts with <div> for regexp reuse
* combine posts on situated schema book on git



## Chapters
<!-- write at the end --> 











<!-- ## References in the Behavior literature

1. social cognition
1. cognitive dimensions
1. narrative and consciousness
1. cambridge handbook
1. creativity
1. social psychology of expertise
1. radicalizing enactivism
1. science of stories
1. psychology the key concepts
1. the bounds of reason
1. cognition, evolution and behavior
1. collective animal behavior
1. enaction: toward a new paradigm



## References situated measurement

1. bayesian
1. programming
1. simulation and bootstrapping 
1. python hard way
1. Hadley programming and Rcpp
 -->

<!-- ## Components


* enacted cog: agile, 3d hardware
* embedded cog: immersion, narrative, music, other beings (biological or biotech robots or combined)
* embodied cog: music, narrative, biotech of emotions, biotech robots and emotion
* extended coggit: programming, social networks
* classical cognition
    * [game theory](https://www.coursera.org/course/gametheory)
    * [calculus](https://www.khanacademy.org/math/calculus)
    * [linear algebra](https://www.khanacademy.org/math/linear-algebra)
    * set theory and [logic](https://www.coursera.org/course/intrologic)
    * ontologies - `<div>` for content models
* situated measurement
    * IRT/CAT
    * pricing analytics and behavior
    * PR behavior and social networks
 -->


<!--  -->
 
