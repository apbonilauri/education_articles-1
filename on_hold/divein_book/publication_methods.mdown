## Methods

<!-- need to create alerts for each of the methods areas -->

### Situation as a common denominator across all designs
The main principle behind the use of these methods is that all of them are connected to the analysis of situations. Examples include:

* For item banks situations will be included as factors potentially leading toward DIF
* For randomized trials situations will be considered as strata, demonstrating that the situation can substantially change
* For SEM and mixture modeling situations will serve as a stratifying variable, allowing for a comparison between structures and parameters between different situations
* For CTA-SCA the expectation is that different situations would bring up different CTA-SCA since people would use different SCA and maybe even CTA. In other words, the same people would think differently under different scenarios, which is the basis for the introduction of trading zones. 

### Item banks and computerized adaptive tests
* Situational evaluation
  * Xavier: intro/extroversion and in/stability
  * Socialization/individualization bank <!--https://www.filepicker.io/api/file/TLU0mxQKaIVR2C6azVwK -->  
  * [Herzberg's motivation-hygiene theory](http://en.wikipedia.org/wiki/Two_factor_theory)
  * [Management style](http://www.nwlink.com/~donclark/leader/obsurvey.html)
* Automatic item generation and quality metrics in situational contexts
  * situations extending the life of individual items since they alter the construct being measured
* Situational DIF
  * demonstrate that the same item can present DIF in some situations but not in others
* Explore other possibilities in the context of [Elements of Adaptive Testing](https://docs.google.com/file/d/0B4Ke-17mTW1_c3d1S2ExaVctNlk/edit)

### Situational randomized trials
* Comparing different methods to achieve immersion and coaching efficacy
* Use Bayesian adaptive trials in the context of sequential 
* n-of-1 for subsequent interventions across modules for a single course
* factorial trials
* cross-over trials
* cluster trials involving groups of students working together
* Pragmatic trials within course secondary to large numbers, validating course situation in relation to target situation
* [Sequential trial](http://en.wikipedia.org/wiki/Sequential_analysis)

### SEM and mixture modeling
* Psychological constructs and learning outcomes
* Use social network data (Facebook and Linked-In) for segmentation
* Segmentation using latent variables (mixture modeling)

### CTA combined with SCA
* visual cognitive schemata connected with manner by which course is prepared - [fuzzy cognitive mapping](https://docs.google.com/file/d/0B4Ke-17mTW1_TUJIY0xKX1B1eFE/edit)
* CTA elicitation within a given situation as determined by SCA - think aloud protocol and handicap analysis

### Agile course preparation
* iterative cycles and MVP (minimum viable product)

## Interns
* Trainers/researchers can make money and publish at the same time
* Establish connections with professors in American universities so that they can publish while working on a research fellowship
* Establish connections with universities providing certificates for PreciseSkills

## Collaborators

Main proposal is to approach a professor with the following:

1. Article in progress based on PS data in support of an SBIR grant proposal - clearly specify his/her role as a supervisor
2. Propose that we could bring in an international research fellow to keep working on the program

Potential collaborators include:

* UNC-CH
  * http://goo.gl/Rfxz7
  * http://goo.gl/0Drac
  * http://goo.gl/Ui0Jn
  * http://goo.gl/AIhpS
* NCSU
  * http://goo.gl/IvVVD
  * http://goo.gl/SlXJ5
  * http://goo.gl/bMUZN
  * http://goo.gl/cJeR7
  * http://goo.gl/QfRC4
  * http://goo.gl/5VVEz
  * http://goo.gl/s3gbD
  * http://goo.gl/kzKLX
  * http://goo.gl/HkbSE
* UNC-G - http://goo.gl/k14Tu
  * Randy Penfield
  * Terry Ackerman
  * Robert Henson
  * Ric Luecht
  * Devdass Sunnassee
  * John Willse